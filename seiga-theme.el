;;; seiga-theme.el --- Seiga Kaku personal theme.  -*- lexical-binding: t; -*-

;; Copyright (c) 2024 Seiga Kaku

(deftheme seiga
  "Seiga Kaku personal theme")

(let ((fg "gray") (dim-fg "slate gray")
      (bg "black"))

  (custom-theme-set-faces
   'seiga
   `(default ((t (:foreground ,fg :background ,bg))))
   `(fringe ((t (:foreground "light gray" :background ,bg))))
   `(highlight ((t (:background "gray9"))))
   `(region ((t (:background "SlateGray"))))

   `(cursor ((t (:background "salmon"))))

   `(mode-line ((t (:background ,bg :box (:line-width -1 :style released-button)))))
   `(mode-line-inactive ((t (:foreground ,dim-fg :background ,bg :box (:line-width -1 :style released-button)))))

   `(header-line ((t (:foreground ,fg :background ,bg))))
   `(header-line-highlight ((t (:foreground ,fg :background ,bg :box (:line-width -1 :style released-button)))))

   `(font-lock-comment-face ((t (:foreground ,dim-fg))))
   `(font-lock-string-face ((t (:foreground "plum"))))
   `(font-lock-number-face ((t (:foreground ,fg))))
   `(font-lock-builtin-face ((t (:foreground "dark violet"))))
   `(font-lock-keyword-face ((t (:foreground "deep pink"))))
   `(font-lock-type-face ((t (:foreground "pale violet red"))))
   `(font-lock-function-name-face ((t (:foreground ,fg :weight bold))))
   `(font-lock-variable-name-face ((t (:foreground ,fg :weight bold))))
   `(font-lock-doc-face ((t (:foreground "thistle" :slant italic))))
   `(font-lock-constant-face ((t (:foreground "maroon"))))
   `(font-lock-property-name-face ((t (:foreground "firebrick"))))

   `(orderless-match-face-0 ((t (:foreground "maroon"))))
   `(orderless-match-face-1 ((t (:foreground "deep pink"))))
   `(orderless-match-face-2 ((t (:foreground "purple"))))
   `(orderless-match-face-3 ((t (:foreground "dark orchid"))))

   `(sly-dynhl-builtin-face ((t (:foreground "#B80458"))))

   `(sly-dynhl-function-name-face ((t (:foreground "#F26398"))))
   `(sly-dynhl-function-call-face ((t (:foreground "#bebebe"))))

   `(sly-dynhl-macro-name-face ((t (:foreground "#B80458"))))
   `(sly-dynhl-macro-call-face ((t (:foreground "#d3d3d3" :weight bold))))

   `(sly-dynhl-doc-face ((t (:foreground "slate gray"))))

   `(sly-dynhl-generic-name-face ((t (:foreground "#B4121A"))))
   `(sly-dynhl-method-name-face ((t (:foreground "#D6253F"))))
   `(sly-dynhl-method-call-face ((t (:foreground "#f5f5f5"))))

   `(sly-dynhl-keyword-face ((t (:foreground "#FF4A00"))))
   `(sly-dynhl-uninterned-face ((t (:foreground "#9D2200"))))

   `(sly-dynhl-ampersand-symbol-face ((t (:foreground "#04BF9D"))))
   `(sly-dynhl-optimization-policy-face ((t (:inherit sly-dynhl-declamation-face))))
   `(sly-dynhl-optimization-level-face ((t (:foreground "#04BFBF"))))
   `(sly-dynhl-optimization-level-0-face ((t (:foreground "#FFFFFF"))))
   `(sly-dynhl-optimization-level-1-face ((t (:foreground "#0143E1"))))
   `(sly-dynhl-optimization-level-2-face ((t (:foreground "#007AE1"))))
   `(sly-dynhl-optimization-level-3-face ((t (:foreground "#03A9E0"))))

   `(sly-dynhl-nil-face ((t (:foreground "#E81E43"))))
   `(sly-dynhl-t-face ((t (:foreground "#C5A206"))))
   `(sly-dynhl-type-face ((t (:foreground "#7E24F4"))))

   `(sly-mrepl-prompt-face ((t (:inherit default))))
   `(sly-mrepl-note-face ((t (:foreground ,dim-fg))))
   `(sly-mrepl-output-face ((t (:foreground "deep pink"))))
   `(sly-part-button-face ((t (:foreground "light salmon"))))

   `(corfu-current ((t (:foreground ,fg :background "dark red"))))

   `(eshell-ls-executable ((t (:foreground "light sea green"))))))

;;;###autoload
(when (and (boundp 'custom-theme-load-path) load-file-name)
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'seiga)

;;; seiga-theme.el
